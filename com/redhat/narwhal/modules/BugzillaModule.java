package com.redhat.narwhal.modules;

import com.redhat.narwhal.Module;
import com.redhat.narwhal.fields.DateType;
import com.redhat.narwhal.fields.Field;
import com.redhat.narwhal.fields.PersonType;


public class BugzillaModule extends Module {

    private RequestHandler requestHandler;

    public class RequestHandler extends Module.RequestHandler {
        public boolean helloWorld() {
            System.out.println("Hello World!");
            return true;
        }
    }
    
    public BugzillaModule () {
    	super();
        this.requestHandler = new RequestHandler();
        this.registerField(new Field("Creator", new PersonType()));
        this.registerField(new Field("CreationDate", new DateType()));
    }

    public RequestHandler getRequestHandler() {
        return this.requestHandler;
    }

}
