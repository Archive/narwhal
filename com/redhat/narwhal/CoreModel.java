package com.redhat.narwhal;

import java.util.Map;

import com.redhat.narwhal.fields.Field;


public class CoreModel extends Model {

    public CoreModel(Store store, CoreModule module) {
        super(store, "core", module.getFields());
    }

    public Task addTaskToTable(Map<Field, Object> initialValues) {
        return this.store.createNewTask(initialValues);
    }

}
