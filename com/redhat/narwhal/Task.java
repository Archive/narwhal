package com.redhat.narwhal;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Task {
    URI taskServer;
    int taskID;
    boolean localTask;
    
    public static ArrayList<Task> tasksFromTaskIDs(Collection taskIDs) {
        ArrayList<Task> tasks = new ArrayList<Task>();
        
        Iterator i = taskIDs.iterator();
        while (i.hasNext()) {
            Integer taskID = (Integer)i.next();
            tasks.add(new Task(taskID.intValue()));
        }
        return tasks;   
    }
    
    public Task(int taskID) {
        this.taskID = taskID;
        this.localTask = true;
        try {
            this.taskServer = new URI("http://localhost:18284/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
    
    public Task(URI taskServer, int taskID) {
        this.taskID = taskID;
        this.taskServer = taskServer;
        this.localTask = false;
    }
    
    public URI getTaskURI() {
        Integer taskID = new Integer(this.taskID);
        return this.taskServer.resolve(taskID.toString());
    }
    
    public int getTaskID() {
        return this.taskID;
    }
    
    public boolean isLocal() {
    	return this.localTask;
    }
    
    public boolean equals(Object object) {
        if (Task.class.isInstance(object)) {
            Task task = (Task)object;
            return this.getTaskURI().equals(task.getTaskURI());
        }
        return false;
    }
    
    public int hashCode() {
        return this.taskID;
    }
    
    public String toString() {
        return this.getTaskURI().toString();
    }
    
}
