package com.redhat.narwhal.fields;

public abstract class Type {

	public abstract String getName();
	public abstract String getPostgresName();

	public boolean equals (Object object) {
	    if (Type.class.isInstance(object)) {
	        Type type = (Type)object;
	        return this.getName().equals(type.getName());
	    }
	    return false;
	}
	public int hashCode() {
	    return this.getName().hashCode();
	}
}
