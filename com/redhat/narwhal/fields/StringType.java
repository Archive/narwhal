package com.redhat.narwhal.fields;

public class StringType extends Type {

	public String getName() {
		return "String";
	}

	public String getPostgresName() {
		return "text";
	}

}
