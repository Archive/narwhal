package com.redhat.narwhal.fields;


public class Field {

	private String name;
	private Type type;

	public Field(String name, Type type) {
		this.name = name;
		this.type = type;
	}	
	
	public String getName() {
		return this.name;
	}

	public Type getType() {
		return this.type;
	}
	
	public boolean equals(Object object) {
	    if (Field.class.isInstance(object)) {
	        Field field = (Field)object;
	        if (this.getType().equals(field.getType())) {
	            return this.getName().equals(field.getName());
	        }
	    }
	    return false;
	}
	
	public int hashCode() {
	    return this.getName().hashCode();
	}

}
