package com.redhat.narwhal.fields;

public class PersonType extends Type {

    public String getName() {
        return "Person";
    }

    public String getPostgresName() {
        return "text";
    }

}
