package com.redhat.narwhal.fields;

public class DateType extends Type {

    public String getName() {
        return "Date";
    }

    public String getPostgresName() {
        return "timestamp";
    }

}
