package com.redhat.narwhal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import com.redhat.narwhal.fields.Field;
import com.redhat.narwhal.fields.PersonType;
import com.redhat.narwhal.fields.StringType;

public final class CoreModule extends Module {

    private RequestHandler requestHandler;
    private CoreModel coreModel;
	private ModuleManager moduleManager;
    
    public class RequestHandler extends Module.RequestHandler {
        CoreModule module;
        
        RequestHandler(CoreModule module) {
            this.module = module;
        }
        
        public int newTask(String owner, String title) {
            try {
                HashMap<Field, Object> initialValues = new HashMap<Field, Object>();
                initialValues.put(new Field("Owner", new PersonType()), owner);
                initialValues.put(new Field("Title", new StringType()), title);
                Task task = this.module.getModel().addTaskToTable(initialValues);
                return task.getTaskID();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        }
        
        public Hashtable getFieldsForTasks(Vector taskIDs, Vector fieldNames) {
            try {
                ArrayList<Task> tasks = Task.tasksFromTaskIDs(taskIDs);
                ArrayList<Field> fields = this.module.fieldNamesToFields(fieldNames);
                HashMap<Task, HashMap<Field, Object>> values = this.module.getModel().getValues(tasks, fields);
                
                
                Hashtable<String, Hashtable<String, Object>> taskToFields = new Hashtable<String, Hashtable<String, Object>>();
                
                for (Task task : tasks) {
                	Hashtable<String, Object> taskFields = new Hashtable<String, Object>();
                	HashMap<Field, Object> value = values.get(task);
                	for (Field field : value.keySet()) {
                		taskFields.put(field.getName(), value.get(field));
                	}
                	
                	taskToFields.put((new Integer(task.getTaskID())).toString(), taskFields);
                    
                }
                System.out.println("Success, returning hashtable");
                return taskToFields;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        
        public Vector getTasksOwnedBy(String person) {
            try {
                Field field = new Field("Owner", new PersonType());
                ArrayList<Task> tasks = this.module.getModel().getTasksWithFieldContaining(field, person);
                Vector<Integer> vector = new Vector<Integer>();
                for (Task task : tasks) {
                    vector.add(new Integer(task.getTaskID()));
                }
                return vector;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        
        public Vector getModules(int taskID) {
        	// FIXME: don't ignore taskID, but for now all have the
        	// same modules, so it doesn't matter
        	Collection<Module> modules = moduleManager.getLoadedModules();
        	Vector<String> moduleNames = new Vector<String>();
        	for (Module module : modules) {
        		moduleNames.add(module.getClass().getCanonicalName());
        	}
        	return moduleNames;
        }
        
        private Map<Task,Map<Field,Object>> buildTaskToFieldValues(Hashtable inValue) {
        	Map<Task,Map<Field,Object>> taskToFieldValues = new HashMap<Task,Map<Field,Object>>();
        	
        	for (Object key : inValue.keySet()) {
        		Task task = new Task((new Integer((String)key)).intValue());
        		Map<Field,Object> fieldValue = new HashMap<Field, Object>();
        		
        		Hashtable keyValues = (Hashtable)inValue.get(key);
        		for (Object fieldKey : keyValues.keySet()) {
        			String fieldName = (String)fieldKey;
        			Field field = this.module.getFieldNamed(fieldName);
        			fieldValue.put(field, keyValues.get(fieldKey));
        		}
        		
        		taskToFieldValues.put(task, fieldValue);
        	}
        	return taskToFieldValues;
        }
        
        public boolean updateTask(Hashtable inValue) {
        	Map<Task,Map<Field,Object>> taskToFieldValues = this.buildTaskToFieldValues(inValue);
        	
        	this.module.getModel().updateTasks(taskToFieldValues);
        	
        	return false;
        }
    }
    
    public CoreModule (ModuleManager moduleManager) {
    	super();
    	this.registerField(new Field("Owner", new PersonType()));
    	this.registerField(new Field("Title", new StringType()));
        this.requestHandler = new RequestHandler(this);
        this.moduleManager = moduleManager;
    }

    public CoreModel getModel() {
        return this.coreModel;
    }    
    
    public void setModel(CoreModel model) {
        System.out.println("Setting model to " + model.toString());
        super.setModel(model);
        this.coreModel = model;
    }

    public RequestHandler getRequestHandler() {
        return this.requestHandler;
    }

}
