package com.redhat.narwhal;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


class ModuleManager {
	
	private Store store;
    private RPCHandler rpcHandler;
    private ArrayList<Module> modules;
    private CoreModule coreModule;
    
	class CannotLoadModule extends Exception {
		private static final long serialVersionUID = 1L;
		String moduleName;
		
		CannotLoadModule (String moduleName) {
			this.moduleName = moduleName;
		}
	}
	
	protected ModuleManager (Store store, List<String> modules) {
		this.store = store;
		this.rpcHandler = new RPCHandler();
		this.modules = new ArrayList<Module>();
		
		this.loadCoreModule();
		
		Iterator<String> i = modules.iterator();
		while (i.hasNext()) {
			try {
				Module loadedModule = this.loadModule(i.next());
				this.modules.add(loadedModule);
			} catch (CannotLoadModule e) {
				System.out.println("Cannot load module");
			}
		}
	}
	
	private void loadCoreModule () {
	    System.out.println("Loading core module");
	    this.coreModule = new CoreModule(this);
	    CoreModel coreModel = new CoreModel(this.store, coreModule);
	    this.coreModule.setModel(coreModel);

	    this.rpcHandler.shareModule(this.coreModule, "tasks");
	}
	
	private Module loadModule (String moduleName) throws CannotLoadModule {
		Module module;
		try {
			Class moduleClass;
			moduleClass = Class.forName(moduleName);
			module = (Module) (moduleClass.newInstance());
		} catch (Exception e) {
			throw new CannotLoadModule(moduleName);
		}
		
		Model model = new Model(this.store, module);
		module.setModel(model);
		
		this.rpcHandler.shareModule(module);
		
		return module;
	}
	
	public ArrayList<Module> getLoadedModules() {
		return this.modules;
	}
}
