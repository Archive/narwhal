package com.redhat.narwhal;

import org.apache.xmlrpc.WebServer;

public class RPCHandler {

    WebServer server;
    
    RPCHandler() {
        System.out.println("Starting server");
        server = new WebServer(18284);
        server.start();        
    }
    
    public void shareModule(Module module, String name) {
        System.out.println("Registering handler for " + name);
        server.addHandler(name, module.getRequestHandler());  
    }
    
    public void shareModule(Module module) {
        String moduleName = module.getClass().getCanonicalName();
        this.shareModule(module, moduleName);
    }
}
