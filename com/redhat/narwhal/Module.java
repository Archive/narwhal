package com.redhat.narwhal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import com.redhat.narwhal.fields.Field;

public abstract class Module {
    private Model model;
    private HashMap<String, Field> fieldNameToField;
    private ArrayList<Field> fields;
    
    public abstract class RequestHandler {
    }
     
    public abstract RequestHandler getRequestHandler();
    
    protected Module() {
    	this.fieldNameToField = new HashMap<String, Field>();
    	this.fields = new ArrayList<Field>();
    }
    
    public Model getModel() {
        return this.model;
    }
    
	public void setModel(Model model) {
	    this.model = model;
	}
	
	public ArrayList<Field> getFields() {
		return this.fields;
	}

	protected void registerField(Field field) {
		this.fields.add(field);
		this.fieldNameToField.put(field.getName(), field);
	}
	
	protected Field getFieldNamed(String fieldName) {
		return this.fieldNameToField.get(fieldName);
	}
	
	protected ArrayList<Field> fieldNamesToFields(Vector fieldNames) {
		ArrayList<Field> fields = new ArrayList<Field>();
		Iterator i = fieldNames.iterator();
		while (i.hasNext()) {
			String fieldName = (String)i.next();
			fields.add(this.getFieldNamed(fieldName));
		}
		return fields;
	}

}
