package com.redhat.narwhal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.redhat.narwhal.fields.Field;

public class Store {
	
	Connection db;
	
	public Store (String url, Properties dbConnectionProps) {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Failed to load");
		}
		
		try {
			this.db = DriverManager.getConnection(url, dbConnectionProps);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean tableExists(String string) {
		return false;
	}
	
	public void createTable(String tableName, List<Field> fields) {		
		String fieldList = " (taskID int, " + StoreUtils.getCommaSeparatedPostgresTypeList(fields) + ")";

		try {
			Statement statement = this.db.createStatement();
		    String dropCommand = "DROP TABLE " + tableName;
		    statement.execute(dropCommand);
		    statement.close();
		} catch (SQLException e1) {
			System.err.println("Couldn't drop table " + tableName + ", table may not exist yet");
		}
		
		try {
			Statement statement = this.db.createStatement();
		    String createCommand = "CREATE TABLE " + tableName + fieldList;
			System.out.println("Executing " + createCommand);
			statement.execute(createCommand);
			statement.close();			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

	}
	
	private HashMap<Field, Object> getValuesForRow(ResultSet resultSet, ArrayList<Field> fields) {
	    HashMap<Field, Object> values = new HashMap<Field, Object>();
	    
	    for (Field field : fields) {
	        try {
	            Object object = resultSet.getObject(field.getName());
	            values.put(field, object);
	            System.out.println("Field: " + field + "  Object: " + object);
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
	    
	    return values;
	}
	
	public HashMap<Task, HashMap<Field, Object>> getValues(String tableName, Collection<Task> tasks, ArrayList<Field> fields) {
        String query = "SELECT taskID, " + StoreUtils.getCommaSeparatedFieldNames(fields) + " FROM " + tableName + " WHERE taskID in (" + StoreUtils.getCommaSeparatedTasks(tasks) + ")";
        
        HashMap<Task, HashMap<Field, Object>> results = new HashMap<Task, HashMap<Field, Object>>();
        
		try {
			Statement statement = this.db.createStatement();
			System.out.println("Running " + query);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
			    Task task = new Task(resultSet.getInt("taskID"));
			    System.out.println("Getting row " + task.getTaskID());
			    results.put(task, this.getValuesForRow(resultSet, fields));
			}
			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}        
		return results;
	}
	
	public void updateTask(String tableName, Task task, Map<Field, Object> fields) {
	    String query = "UPDATE " + tableName + " SET ";
	    boolean first = true;
	    for (Field field : fields.keySet()) {
	        if (!first) {
	            query += ", ";
	        } else {
	            first = false;
	        }	        
	        Object object = fields.get(field);
	        query += field.getName() + " = \'" + object.toString() + "\'";
	    }
	    query += " WHERE taskID = " + (new Integer(task.getTaskID())).toString();
	    
		try {
            Statement statement = this.db.createStatement();
            System.out.println("About to run " + query);
            statement.execute(query);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }	
	}
	
	public void addTaskToTable(String tableName, Task task, Map<Field, Object> fields) {
	    try {
            Statement statement = this.db.createStatement();
            addTaskToTable(statement, tableName, task, fields);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	private void addTaskToTable(Statement statement, String tableName, Task task, Map<Field, Object> initialValues) throws SQLException {
	    String columnList = " (taskID";
	    String valueList = "(" + (new Integer(task.getTaskID()).toString());
	    for (Field field : initialValues.keySet()) {      
	        Object object = initialValues.get(field);
	        columnList += ", " + field.getName();
	        valueList += ", \'" + object.toString() + "\'";	        
	    }
	    columnList += ")";
	    valueList += ")";
	    
	    String command = "INSERT INTO " + tableName + columnList + " VALUES " + valueList;
	    System.out.println("About to run " + command);
	    statement.execute(command);
	}
	
    public Task createNewTask(Map<Field, Object> initialValues) {
        Task task;

        ResultSet resultSet;
        Statement statement = null;
        try {
            statement = this.db.createStatement();
            resultSet = statement.executeQuery("SELECT MAX(taskID) from core");
			resultSet.next();
			task = new Task(resultSet.getInt(1) + 1);
			resultSet.close();
		} catch (SQLException e) {
		    task = new Task(15);
		}       
		
		try {
			this.addTaskToTable(statement, "core", task, initialValues);
			statement.close();
		} catch (SQLException e) {
		    e.printStackTrace();
		}           
		return task;
    }

    public ArrayList<Task> getTasksFromResultSet(ResultSet resultSet) throws SQLException {
        ArrayList<Task> results = new ArrayList<Task>();
        while (resultSet.next()) {
            results.add(new Task(resultSet.getInt("taskID")));
        }
        return results;
    }
    
    public ArrayList<Task> getTasksWithColumnContaining(String tableName, String column, String match) {
        try {
            String query = "SELECT taskID from " + tableName + " WHERE " + column + " LIKE \'%" + match + "%\'"; 
            Statement statement = this.db.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            ArrayList<Task> results = this.getTasksFromResultSet(resultSet);
			resultSet.close();
			statement.close();
			return results;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
	
}
