package com.redhat.narwhal;

import java.util.Collection;
import java.util.List;

import com.redhat.narwhal.fields.Field;

public class StoreUtils {

	/* Produces "name, name, name..." */
	public static String getCommaSeparatedFieldNames(List<Field> fields) {
		String result = null;

		for (Field field : fields) {
			if (result == null) {
				result = field.getName();
			} else {
				result += ", " + field.getName();
			}
		}
		
		if (result == null) result = "";
		return result;
	}
	
	/* Produces: "name type, name type, name type,..." */
	public static String getCommaSeparatedPostgresTypeList(List<Field> fields) {
		String result = "";
		boolean first = true;
		
		for (Field field : fields) {
			if (first) {
				first = false;
			} else {
				result += ", ";
			}
			result += field.getName() + " " + field.getType().getPostgresName();
		}
		return result;
	}
	
	/* Produces: "task, task, task,..." */
	public static String getCommaSeparatedTasks(Collection<Task> tasks) {
		String result = "";
		boolean first = true;
		for (Task task : tasks) {
			if (!first) {
				result += ",";
			} else {
				first = false;
			}
			
			result += (new Integer(task.getTaskID())).toString();
		}
		return result;
	}
}
