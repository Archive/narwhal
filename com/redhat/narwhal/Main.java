package com.redhat.narwhal;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

class Main {
    public static void main (String [] args) {
        List<String> modules = new ArrayList<String>();
        
        modules.add(new String("com.redhat.narwhal.modules.BugzillaModule"));
        
        String propsFile = System.getProperty("user.home") + 
        	System.getProperty("file.separator") + ".narwhal.properties";
        Properties props = new Properties();
        try {
			InputStream fio = new FileInputStream(propsFile);
			try {
				props.load(fio);
				if (null != fio) {
					fio.close();
					fio = null;
				}
			} catch (IOException ignore) { 
				if (null != fio) {
					try {
						fio.close();
					} catch (IOException ignoreAnother) {}
					fio = null;
				}
			}
		} catch (FileNotFoundException ignore) { }
		
        String dbType = props.getProperty("dbType","postgresql");
        String host   = props.getProperty("host","localhost");
        String dbName = props.getProperty("dbName","narwhal");
        String user   = props.getProperty("user", "clarkbw");
        
        String url = "jdbc:"+dbType+"://"+host+"/"+dbName;
        props.setProperty("user", user);

        Store store = new Store(url, props);
        
        ModuleManager moduleManager = new ModuleManager(store, modules);
        
        try {
			props.store(new FileOutputStream(propsFile), "Saving Properties for no good reason");
		} catch (FileNotFoundException ignore) {
		} catch (IOException ignore) { }
        props = null;
    }
    
}
