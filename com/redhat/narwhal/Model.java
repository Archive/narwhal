package com.redhat.narwhal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.redhat.narwhal.fields.Field;


public class Model {
	protected Store store;
	protected String tableName;
	protected ArrayList<Field> fields;
	
	protected Model (Store store, String tableName, ArrayList<Field> fields) {
	    this.store = store;
	    this.tableName = tableName;
	    this.fields = fields;
	    
		if (store.tableExists(tableName)) {
			/* FIXME: make sure existing table's signature matches modules...
			 * repair if needed?
			 */
		} else {
			store.createTable(tableName, fields);
		}	    
	}
	
	public Model (Store store, Module module) {
		this(store, buildTableName(module), module.getFields());	
	}

    private static String buildTableName(Module module) {
        String moduleName = module.getClass().getCanonicalName();
		return "m_" + moduleName.replace('.', '_');
	}
    
    public HashMap<Task, HashMap<Field, Object>> getValues(Collection<Task> tasks) {
        return this.getValues(tasks, this.fields);
    }
    
    public HashMap<Task, HashMap<Field, Object>> getValues(Collection<Task> tasks, ArrayList<Field> fields) {
        return this.store.getValues(this.tableName, tasks, fields);
    }
    
    public void updateTasks(Map<Task, Map<Field, Object>> taskToFieldValues) {
    	for (Task task : taskToFieldValues.keySet()) {
    		this.store.updateTask(this.tableName, task, taskToFieldValues.get(task));
    	}
    }
    public ArrayList<Task> getTasksWithFieldContaining(Field field, String match) {
        return this.store.getTasksWithColumnContaining(this.tableName, field.getName(), match);
    }
    
    public void enableModuleForTask(Task task) {
    }
}