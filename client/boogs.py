#!/usr/bin/env python

import gtk, gtk.glade, gtk.gdk
import gconf
import gobject
import sqlite
from datetime import datetime
from xmlrpclib import Server

GCONF_PREFIX="/apps/boogs"
GCONF_PASSWORD="/password"
GCONF_USERNAME="/username"
GCONF_VIEW_LIST="/views"
GCONF_SERVER="/server_url"

class boogs:

    def __init__(self):

        self.__init_gconf__()
        self.__init_db__()
        self.__init_auth__()

        self.win=gtk.glade.XML ("boogs.glade","boogs")
        
        dic = { "on_boogs_destroy_event": self.quit,
                "on_boogs_delete_event": self.quit,
                "on_search_button_clicked": self.search }

        self.win.signal_autoconnect (dic)
        
        self.q = self.win.get_widget ("search_entry")
        self.__init_search_completion__()
        
        self.__init_tasks__()
        self.__init_views__()

        self.__init_xmlrpc__()

        gobject.idle_add(self.sync_bugs)

    def sync_bugs(self):
        owner = self.gconf.get_string(GCONF_PREFIX + GCONF_USERNAME)
        print "debug"
        tasks = self.xmlrpc.tasks.getTasksOwnedBy("bclark")

        titles = self.xmlrpc.tasks.getTitles(tasks)
        for i in range(len(titles)):
            self.model.append([titles[i],"bclark",tasks[i]])
    
    def search(self, widget):
        print self.q.get_text()
        self._add_search_term(self.q.get_text())

    def __init_search_completion__(self):
        search_completion = gtk.EntryCompletion()
        self.search_model = gtk.ListStore (gobject.TYPE_STRING)
        search_completion.set_model(self.search_model)
        search_completion.set_text_column(0)
        self.q.set_completion(search_completion)

        # rebuild list of searches
        cu = self.db_conn.cursor()
        cu.execute("select term from search_terms")
        import string
        for row in cu.fetchall():
            self.search_model.append([string.strip(row[0],"'")])

    def _add_search_term(self, term):
        try:
            # try to save a search term
            cu = self.db_conn.cursor()
            cu.execute("insert into search_terms (term,used) " +
                       "values (\"%s\",\"%s\")",
                       (term, datetime.now()))
            self.db_conn.commit()

            self.search_model.append([term])

        except sqlite.IntegrityError, e:
            try:
                # if the term is already saved, update its timestamp
                cu.execute("update search_terms set used=\"%s\" where term=%s",
                           (datetime.now(),term))
                self.db_conn.commit()                
            except:
                pass

    def __init_xmlrpc__(self):
        if not self.gconf.get_string(GCONF_PREFIX + GCONF_SERVER):
            self.gconf.set_string(GCONF_PREFIX + GCONF_SERVER,
                                  "http://localhost:18284")
        self.xmlrpc = Server(self.gconf.get_string(GCONF_PREFIX +
                                                   GCONF_SERVER))
        
    def __init_gconf__(self):
        self.gconf = gconf.client_get_default()
        self.gconf.add_dir(GCONF_PREFIX, gconf.CLIENT_PRELOAD_ONELEVEL)

    def __init_db__(self):
        import os
        db_path = os.path.expanduser("~/.gnome2/boogs")

        if not os.access(db_path, os.F_OK):
            os.makedirs(db_path, 0777)
            
        self.db_file = os.path.join(db_path,'db')
        
        if not os.access(self.db_file, os.F_OK):
            init_schema = True
        else:
            init_schema = False
            
        self.db_conn = sqlite.connect(self.db_file)

        if init_schema :
            cu = self.db_conn.cursor()
            cu.execute("""
            create table search_terms (
            term text primary key,
            used timestamp not null
            )
            """)
            self.db_conn.commit()
            
    def __init_auth__(self):
        username = self.gconf.get_string(GCONF_PREFIX + "/username")
        if username and len(username) > 0:
            return
        else:
            self.auth=gtk.glade.XML ("boogs.glade","auth_window")
            self.u_entry = self.auth.get_widget("username_entry")
            self.p_entry = self.auth.get_widget("password_entry")
            u = self.gconf.get_string(GCONF_PREFIX + GCONF_USERNAME)
            if u:
                self.u_entry.set_text(u)
            p = self.gconf.get_string(GCONF_PREFIX + GCONF_PASSWORD)
            if p:
                self.p_entry.set_text(p)
            dic = { "on_auth_window_destroy_event": self.quit,
                    "on_auth_window_delete_event": self.quit,
                    "on_cancel_button_clicked": self.quit,
                    "on_login_button_clicked": self.save_auth }
            self.auth.signal_autoconnect (dic)

    def save_auth(self, widget):
        self.gconf.set_string(GCONF_PREFIX + GCONF_USERNAME,
                              self.u_entry.get_text())
        self.gconf.set_string(GCONF_PREFIX + GCONF_PASSWORD,
                              self.p_entry.get_text())
        if self.u_entry.get_text() and self.p_entry.get_text():
            self.auth.get_widget("auth_window").hide()
            del self.u_entry, self.p_entry
            del self.auth
            gobject.idle_add(self.sync_bugs)            
            return True
        else:
            return False
        
    def __init_tasks__(self):
        

        self.task_list = self.win.get_widget ("task_list")
        self.model = gtk.ListStore (gobject.TYPE_STRING,
                                    gobject.TYPE_STRING,
                                    gobject.TYPE_STRING)
        self.task_list.set_model (self.model)

        renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn ('Summary', renderer, text=0)
        self.task_list.append_column (column)

        renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn ('Person', renderer, text=1)
        self.task_list.append_column (column)

        renderer = gtk.CellRendererText()
        renderer.set_fixed_size(20,-1)
        column = gtk.TreeViewColumn ('Date', renderer, text=2)
        self.task_list.append_column (column)

        return

    def __init_views__(self):

        self.views = self.win.get_widget ("views")
        self.view_model = gtk.ListStore (gobject.TYPE_STRING)
        self.views.set_model (self.view_model)
        
        renderer = gtk.CellRendererText ()
        column = gtk.TreeViewColumn ('', renderer, text=0)
        self.views.append_column (column)

        views = self.gconf.get_list(GCONF_PREFIX + GCONF_VIEW_LIST,
                                    gconf.VALUE_STRING)
        for view in views:
            self.view_model.append ([view])

        self.views.connect("row_activated", self.views_activated)

        return

    def views_activated(self, treeview, path, column):
        iter = self.view_model.get_iter(path)
        view = self.view_model.get_value(iter, 0)
        print view
        # self.run_query(view['query'])
        # run query against this view
    
    def quit(self, *args):
        self.db_conn.close()
        gtk.main_quit()


if __name__ == "__main__":
    boogs=boogs()
    gtk.main()
